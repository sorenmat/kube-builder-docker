FROM golang
ENV version=1.0.1
ENV arch=amd64

# download the release
RUN curl -L -O https://github.com/kubernetes-sigs/kubebuilder/releases/download/v${version}/kubebuilder_${version}_linux_${arch}.tar.gz

# extract the archive
RUN tar -zxvf kubebuilder_${version}_linux_${arch}.tar.gz
RUN mv kubebuilder_${version}_linux_${arch} /usr/local/kubebuilder

# update your PATH to include /usr/local/kubebuilder/bin
ENV PATH=$PATH:/usr/local/kubebuilder/bin
